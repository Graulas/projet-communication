<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php

    $malusObj = new \stdClass();

    $cuFile = 'cu.json';
    $cuData = file_get_contents($cuFile);
    $cuJson = json_decode($cuData, true); // pas oublier le true pour tableau associatif. Ca change aussi le manière de recup les info
    $cuJson = array_values($cuJson); // ou mettre cette fonction ? ici ou à la fin. Voir l'autre
    $cuJsonLength = count($cuJson);
    #print_r($mJson);
    #echo "<br>" .$mJsonLength. "<br>";

    #echo "<strong>Canaux déjà utilisés : </strong><br>";
    #for($y=0;$y<$cuJsonLength;$y++){
        #echo $cuJson[$y]["cu"]. ", ";
    #}

    $randNum = rand(0,$cuJsonLength-1);

        // le bout de code si dessous recupere le cu supprimé et le stock 
    $malusObj= $cuJson[$randNum];
    $malusFile = 'malus.json';
    $mData = file_get_contents($malusFile);
    $mJson = json_decode($mData, true);
    $mJson[] = $malusObj;
    $mJson = json_encode($mJson, JSON_PRETTY_PRINT);
    file_put_contents('malus.json', $mJson);

    unset($cuJson[$randNum]);
    $cuJson = array_values($cuJson); // ou mettre cette fonction ? debut ou ici
    
    #print_r($mJson);
    #$mJsonLength = count($mJson);
    #echo "<br>" .$mJsonLength;

    #echo "<br><strong>Canaux déjà utilisés et toujours utilisable : </strong><br>";

    // foreach($mJson as $element){
    //     echo $element["cu"]. ", ";
    // }

        // decomment this line to encode
    $cuJson = json_encode($cuJson, JSON_PRETTY_PRINT); // réencode pour supprimer un cu de la liste des cu utilisable.
    file_put_contents('cu.json', $cuJson);


?>
</body>
</html>