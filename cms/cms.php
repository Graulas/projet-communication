<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// ob_start(); 

date_default_timezone_set('Europe/Paris');


$cookie = new \stdClass();

$cookie = $_COOKIE['utilisateur'];

if ($cookie == "tommy" | $cookie == "theophile") {
} else {
    echo "<p>Authentification erroné ou absente. <br> Revenez à la page d'acceuil pour vous reconnecter <a href='index.php'> ICI </a></p>";
    die();
}

include('librairies/Parsedown.php');
$Parsedown = new Parsedown();
$protocole = '../protocol.md';
$content = file_get_contents($protocole);
$content = $Parsedown->text($content);


function Update_message_list(){

    $myfile = 'data.json';
    
    if (filesize($myfile) == 0){
        echo '<div class="message"><p>Aucun message envoyé pour le moment</p></div>';
    }else{
        
        $data = file_get_contents($myfile);
        $json = json_decode($data, true);

        foreach (array_reverse($json) as $message){
            $message_date=date_create();
            $message_timestamp= $message['timestamp'];
            date_timestamp_set($message_date, $message_timestamp);
            $message_time= date_format($message_date,"Y-m-d H:i:s");
            echo '<div class="message"> <tr rowspan="2"> <th><code class="id">'.$message['id'].'</code></th></tr> <tr><th>'.$message_time.'</th> <th>'.$message['datesent'].'</th></tr> <tr><th>'.$message['format'].'</th><th>'.$message['cu'].'</th></tr> <tr><th rowspan="2">'.$message['author'].'</th></tr></div>';
        }
    }

   
    
}
// TODO: mettre en place la possibilité de mettre un fichier image ou audio
//initialisation qui évite une erreur de variable
$myObj = new \stdClass();
$cuObj = new \stdClass();


if (isset($_POST['who']) && isset($_POST['canal_used']) && isset($_POST['appt']) && isset($_POST['datesent']) && isset($_POST['decentralize']) && isset($_POST['format']) && isset($_POST['encryption']) && isset($_POST['content']) && isset($_POST['canal_n1']) && isset($_POST['canal_n2']) && isset($_POST['canal_n3']) ) {
    $dir = "data";

    #$target_dir = "../uploads/";
    #$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    #$uploadOk = 1;
    #$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    #if (isset($_POST["submit"])) {
        #$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        #if ($check !== false) {
            #echo "File is an image - " . $check["mime"] . ".";
            #$uploadOk = 1;
        #} else {
            #echo "File is not an image.";
            #$uploadOk = 0;
        #}
    #}

    $date=date_create();
    $unix_timestamp=date_timestamp_get($date);
    $datesent=$_POST['datesent'].' '.$_POST['appt'];
    // $date_PRETTY = date("d/m/Y");
    // $date_SHORT = date("Ymd");
    // $heure_PRETTY = date("H:i:s");
    // $heure_SHORT = date("His");

    $myObj->id = uniqid('', true); //unique id
    $myObj->timestamp = $unix_timestamp; //date
    $myObj->author = $cookie;
    $myObj->who = $_POST['who']; // qui
    $myObj->datesent = $datesent; // qui
    $myObj->followup = $_POST['followup']; // qui
    $myObj->cu = $_POST['canal_used']; // canal utilisé
    $myObj->dctrlz = $_POST['decentralize']; // decentralisé
    $myObj->format = $_POST['format']; // tag 
    $myObj->encryption = $_POST['encryption']; // encrypté 
    $myObj->content = $_POST['content']; // contenu 
    #if ($_FILES['fileToUpload'] == true) {
        //TODO: renomé avec id et date ou juste date du message.
        #$myObj->file_uploaded = $_FILES['fileToUpload'];
    #} else {
        #$myObj->file_uploaded = "false";
    #}
    $myObj->cn1 = $_POST['canal_n1']; // canal suggéré 1
    $myObj->cn2 = $_POST['canal_n2']; // canal suggéré 2
    $myObj->cn3 = $_POST['canal_n3']; // canal suggéré 3
    
    $cuObj->cu = $_POST['canal_used'];

    // chmod 777 CMS-Archi --> c'est pas terrible comme solution, il faudrait voir comment faire ça autrement.

    // $myfile = fopen("data.json", "w+")or die('erreur lors de l\'ouverture du fichier data.json.'); 
    $myfile = 'data.json';
    $data = file_get_contents($myfile);
    $json = json_decode($data, true);

    $json[] = $myObj;
    $json = json_encode($json, JSON_PRETTY_PRINT);
    file_put_contents('data.json', $json);
    // header('location:index.php');
    date_timestamp_set($date,$unix_timestamp);
    $current_time=date_format($date,"Y-m-d H:i:s");

    $cuFile = "cu.json";
    $cuData = file_get_contents($cuFile);
    $cuJson = json_decode($cuData);
    $cuJson[] = $cuObj;
    $cuJson = json_encode($cuJson, JSON_PRETTY_PRINT);
    file_put_contents('cu.json', $cuJson);
    // unset($_POST); 
    header("Location: cms.php"); 
    exit();
}
// var_dump(json_decode(file_get_contents($myfile, true)));
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>CMS</title>
</head>

<body>
<div class="header">
  <h2>Formulaire d'agrégation de data volontaire</h2>
</div>
    <div class='link'>
        <a href="hub.php" tabindex="1">Hub</a>
        <a href="dataVis.php" tabindex="1">Data Visualitation</a>   
    </div>
    <div class="row">
        <div class="column" class="formulaire">
            <?php
               if(isset($_POST['submit'])){
                echo '<p id="result">Data enregistré le ' .$current_time.'</p>';
                echo 'Enregistrement dans ' .$malusFile;
            }
            ?>
            <form action="cms.php" method="post" enctype='multipart/form-data'>
                <p><label for="who">Qui a envoyé le message</label>
                <input type="text" name="who"></p>

                <p><label for="birthday">Date et Heure de reception</label>
                <input type="date" id="datesent" name="datesent">
                <input type="time" id="appt" name="appt"></p>

                <p><label for="followup">Réponse à quel message (id)</label>
                <input type="text" name="followup"></p>

                <p><label for="canal_used">Canal utilisé</label>
                <input type="text" name="canal_used"></p>

                <p><label>Le service est-il décentralisé ?</label>
                    <ul>
                        <input type="radio" name="decentralize" value="Oui">
                        <label for="decentralize">Oui</label>
                        <input type="radio" name="decentralize" value="Non" checked="checked">
                        <label for="decentralize">Non</label>
                    </ul>
                </p>

                <p><label for="format">Le format du message ?</label>
                    <select name="format" id="tagList">
                        <option>texte</option>
                        <option>vidéo</option>
                        <option>fichier audio</option>
                        <option>appel</option>
                        <option>gif</option>
                        <option>emoji</option>
                    </select>
                </p>

                <p><label>Le message était encrypté ?</label>
                    <ul>
                        <input type="radio" name="encryption" value="Oui">
                        <label for="encryption">Oui</label>
                        <input type="radio" name="encryption" value="Non" checked="checked">
                        <label for="encryption">Non</label>
                    </ul>
                </p>

                <p><label for="content">Contenu</label>
                <textarea name="content" cols="30" rows="10"></textarea></p>

                <!--<p><label for="file_uploaded">Fichier à uploader</label>
                <input type="file" name="fileToUpload" id="fileToUpload"></p>-->

                <p><label for="canal_n1">1er canal suggéré</label>
                <input type="text" name="canal_n1"></p>

                <p><label for="canal_n2">2ème canal suggéré</label>
                <input type="text" name="canal_n2"></p>

                <p><label for="canal_n3">3ème canal suggéré</label>
                <input type="text" name="canal_n3"></p>
                <input type="reset"> 
                <input type="submit" value="Send">
            </form>

        </div>

        <div class="column" class="protocol">
            <p><?= $content ?></p>
            <a href="https://ether.happyngreen.fr/p/HJUoWnzZMR3t6zmBkfOl">Etherpad du projet</a>
        </div>

        <div class="column" class="timeline">
        <h1>Messages précendents</h1>
        <?php
            Update_message_list();
            if(isset($_POST['submit'])){
                Update_message_list();
            }
        ?>
        </div>
    </div>

    <div class="footer">
        Site web artisanal || Tommy Moucheron & Théophile Gervreau-Mercier 2021©
    </div>
</body>
</html>