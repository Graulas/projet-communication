<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $cookie = new \stdClass();
    $cookie = $_COOKIE['utilisateur'];
    if ($cookie == "tommy" | $cookie == "theophile") {
    } else {
        echo "<p>Authentification erroné ou absente. <br> Revenez à la page d'acceuil pour vous reconnecter <a href='index.php'> ICI </a></p>";
        die();
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DataVisualisation</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class='link'>
        <a href="hub.php" tabindex="1">Hub</a>
        <a href="cms.php" tabindex="1">Formulaire</a>
    </div>
    
    <?php

        $myfile = "data.json";
        if (filesize($myfile) == 0){
            echo '<div class="data_message"><p>Aucun message envoyé pour le moment</p></div>';
        }else{
            $data = file_get_contents($myfile);
            $json = json_decode($data, true);
            $jsonLength = count($json);
            // echo "Nombre de message dans data.json : ".$jsonLength. "<br>";
            // $myarray = array();

            // for($y=0; $y<$jsonLength; $y++){
            //     //echo "ID du message " .$y. " : " .$json[$y]["id"]. "<br>";
            //     array_push($myarray, $json[$y]["content"]);
            // }
            // //print_r(array_filter($myarray)); // array_filter => remove element equal to false (empty or NULL)
            // print_r($myarray);

            $cuFile = "cu.json";
            $cudata = file_get_contents($cuFile);
            $cujson = json_decode($cudata, true);
            $cujson = array_values($cujson);
            $cucount = count($cujson);

            // Code ci dessous, (x) = nombre des canaux utiliser
            //      faire en sorte qu'il ne compte pas deux fois les même.
            echo '<div class="cu"><strong><p>Canaux utilisés et encore utilisables ('.$cucount.') :</p></strong>'; 
            for($y=0;$y<$cucount;$y++){
                if($cujson[$y]["cu"]==null){
                    continue;
                }else{
                    echo $cujson[$y]["cu"]. ", ";
                }
            }
            echo "</div>";
    
            $malusFile = "malus.json";
            $mdata = file_get_contents($malusFile);
            $mjson = json_decode($mdata, true);
            $mjson = array_values($mjson);
            $mcount = count($mjson);

            echo '<div class="cublocked"><strong><p class="blocked">Canaux bloqués ('.$mcount.') : </p></strong>';
            for($y=0;$y<$mcount;$y++){
                echo $mjson[$y]["cu"]. ", ";
            }
            echo '</div>';
            ?> 
            <div class='message_content'>
            <?php  
            // ON DOIT CATEGORISER UN MAX pour une analyse automatique
            // ex : top des canaux utilisé, top format, ... et bien plus encore
            foreach($json as $message){
                $m_date=date_create();
                $m_timestamp=$message['timestamp'];
                date_timestamp_set($m_date, $m_timestamp);
                $m_time=date_format($m_date, "Y-m-d H:i:s");
                $m_author=$message['author'];
                $m_content=$message['content'];
                $m_cu=$message['cu'];
                echo '<div class="data_message"> <div class="time">'.$m_time.'</div> <div>'.$m_author.' on <span style=""> '.$m_cu.'</span></div> <div class="content"> '.$m_content.'</div></div>';
            }
        }
    ?>
    </div>
<div class="footer">
        Site web artisanal || Tommy Moucheron & Théophile Gervreau-Mercier 2021©
</div>
</body>
</html>