### PROTOCOLE du projet communication:

- Le projet a pour but de proposer une performances guidée par le protocole ci-dessous ainsi que son archivage.

- La performance consiste d'échanges et communication entre les partipants. 

- L'archivage prend la forme de fichiers .json ainsi qu'un site web comprennant le traitement des données, des visuels et une appréciation personnel de la performance. L'archivage peut être présenté en format papier généré à partir du site web.

- Un générateur d'incovénients spécialement fait pour la performances va simuler des rebondissement fictionnels qui auront des effets sur l'environnement des conversations. 

- Les inconvénients générés par le script empêchent l'utilisation du moyen de communication / plateforme donné autant pour la communication que pour toutes utilisations dans le cadre de la perfomances. Tout cela est bien-sûr fictionel/simulé et donc au bon vouloir des participants.

- Les participants vont tenter de communiquer sur des sujets divers. Ce sont des sujets qui nous tiennent à coeurs lors de la performance.

- les participants sont obligés d'interagir au moins une fois jours pour crée de l'intéraction.

- À chaque message, chaque participant doit remplir les information du formulaire pour enregistré les intéractions entre participants.

- Au bout de la période convenu pendant laquelle la performance a lieu, les participants peuvent faire un compte rendu ou un débrief et ainsi entamé le travail de traitement des datas de l'archivage de la performance. // note: si c'est d'autres que nous deux, effectivement ils ne feront pas ce boulot là, d'où le besoin d'automatisé la procédure un maximum.

